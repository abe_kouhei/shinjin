#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
flag = 0
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                if "正規化代表表記" in mrph.fstring:
                    flag = 1
            if flag == 1:
                print("".join(mrph.midasi for mrph in bnst.mrph_list()))
            flag = 0
        data = ""
