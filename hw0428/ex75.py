#coding: utf-8
from pyknp import KNP
import re

knp = KNP(jumanpp=True)
result = knp.parse("僕は立って望遠鏡で泳いでいる少女を見た。")

#文節のリスト
for bnst in result.bnst_list():
    parent = bnst.parent
    num_c = bnst.bnst_id
    if parent is not None:
        if re.search(r"用言",bnst.fstring):
            child_rep = " ".join(mrph.repname for mrph in bnst.mrph_list())
            if re.search(r"体言",parent.fstring):
                parent_rep = " ".join(mrph.repname for mrph in parent.mrph_list())
                num_p = parent.bnst_id
                num_diff = num_p - num_c
                print(child_rep, "->", parent_rep, num_p - num_c)
            
