#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
flaga = flagb = 0
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                if mrph.hinsi == "名詞":
                    flaga = 1
                if mrph.hinsi == "接尾辞":
                    flagb = 1
            if flaga == flagb == 1:
                print("".join(mrph.midasi for mrph in bnst.mrph_list()))
            flaga = flagb = 0
        data = ""
