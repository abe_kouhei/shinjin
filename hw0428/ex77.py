#coding: utf-8 
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)

A = ""
flag = 0
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = knp.result(data)
        for bnst in result.bnst_list():
            parent = bnst.parent
            if parent is not None:
                if flag == 1:
                    if bnst.mrph_list()[0].hinsi == "名詞":
                        B = bnst.mrph_list()[0].midasi
                        print(A,"の",B)

                if len(bnst.mrph_list()) > 2:
                    if bnst.mrph_list()[-2].hinsi == "名詞"\
                    and bnst.mrph_list()[-1].midasi == "の"\
                    and parent.bnst_id - bnst.bnst_id != 1:
                        A = bnst.mrph_list()[-2].midasi
                        flag = 1
                        print("flag")
                else:
                    flag = 0
                        
        data = ""





