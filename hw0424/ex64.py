#coding: utf-8 
from collections import defaultdict
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()
keitaiso = defaultdict(int)

data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            keitaiso[mrph.genkei] += 1
            data = ""


print(sorted(keitaiso.items(), key=lambda x: x[1]))
