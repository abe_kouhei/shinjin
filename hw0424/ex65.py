#coding: utf-8 
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()
total = 0
predicate = 0

data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            total += 1
            if mrph.hinsi == ("動詞" or "形容詞" or "形容動詞"):
                predicate += 1
                data = ""

print(predicate/total,predicate,"/",total)
