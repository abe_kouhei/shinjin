#coding: utf-8
from pyknp import KNP
import sys

knp = KNP(jumanpp=True)
data = ""
noun = 0
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = knp.result(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                if mrph.hinsi == "名詞":
                    noun += 1
            if noun >= 2:
                print("見出し:", ",".join(mrph.midasi for mrph in bnst.mrph_list()),noun)
            noun = 0
            data = ""
