#coding: utf-8 
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

sahen = ""
flag = 0
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            if flag == 1 and mrph.midasi == ("する" or "できる"):
                print(sahen + mrph.midasi)
                flag = 0
            if mrph.bunrui == "サ変名詞":
                flag = 1
                sahen = mrph.midasi
            else:
                flag = 0
        data = ""
