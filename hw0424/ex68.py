#coding: utf-8
from pyknp import KNP 
import sys

knp = KNP(jumanpp=True)
result = knp.parse(sys.argv[1])

for bnst in result.bnst_list():
    print("".join(mrph.midasi for mrph in bnst.mrph_list()),end = " ")
