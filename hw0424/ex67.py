#coding: utf-8 
from pyknp import Jumanpp
import sys

jumanpp = Jumanpp()

A = ""
flag = 0
data = ""
for line in iter(sys.stdin.readline, ""): # 入力文を1行ずつ読む
    data += line
    if line.strip() == "EOS": # 1文が終わったら解析
        result = jumanpp.result(data)
        for mrph in result.mrph_list():
            if flag == 2 and mrph.hinsi == "名詞":
                print(A + "の" + mrph.midasi)
                flag = 1
            elif flag == 1 and mrph.midasi == "の":
                flag = 2
            elif mrph.hinsi == "名詞":
                flag = 1
                A = mrph.midasi
            else:
                flag = 0 
        data = ""
