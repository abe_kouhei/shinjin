import sys
import re
from collections import defaultdict

keys = defaultdict(int)
bikeys = defaultdict(int)
with open("doc0000000000.txt", 'r') as doc:
    for line in doc:
        #print (line.strip())
        elim1 = re.search(r"<PAGE*",line)
        elim2 = re.search(r"</PAGE*",line)  #不要部分の除外
        if elim1:
            pass
        elif elim2:
            pass
        else:
            words = (line.strip().split(" "))　#単語分割
            kbefore = ""
            for wo in words:
                keys[wo] += 1
                if kbefore != "":
                    bigram = kbefore + " " + wo 
                    bikeys[bigram] += 1
                kbefore = wo
#各単語および各2単語のセットの出現回数を記録
                
total = 0  #sum of freqency of all words
for words in keys:
    total += keys[words]

def bi_poss(wor1,wor2):  #P(b|a) = freq(a b) / freq(a)
    biwords = wor1 + " " + wor2
    poss = bikeys[biwords]/keys[wor1]
    print("possibility:",biwords,"=",poss)
    return poss

print(total)
                
print (keys["man"])
print (bikeys["the man"])

poss_sen = keys[sys.argv[1]]/total

loop = len(sys.argv)
for k in range(2,loop):
    poss_sen *= bi_poss(sys.argv[k-1],sys.argv[k])
        
print(poss_sen)


"""
poss_the_man = (keys["the"]/total)*bi_poss("the","man")*bi_poss("man","is")*bi_\poss("is","in")*bi_poss("in","the")*bi_poss("the","house")

for k,v in sorted(bikeys.items(),key=lambda x: x[1]):
    print (k,v)
"""
