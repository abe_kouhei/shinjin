def bubble(a):  #バブルソート
    n = 0
    for i in range(0,len(a)-1):
        l = 0
        for j in range (0,len(a)-1-n):
            if a[l] > a[l+1]:
                temp = a[l]
                a[l] = a[l+1]
                a[l+1] =temp
            l += 1
        n += 1            #二重ループでバブルソートを実行
        print (a)         #過程の表示

    return a

def quick(a):   #クイックソート
    print (a)
    if len(a) <= 1:
        return a
    target = a[0]   #基準の選び方は適当です
    S = []
    M = []
    L = []          #基準と比べて小さいもの、同じもの、大きいものに分類
    for i in range(0,len(a)):
        if a[i] < target:
            S.append(a[i])
        if a[i] == target:
            M.append(a[i])
        if a[i] > target:
            L.append(a[i])

    return quick(S) + M + quick(L)  #分類したものを再帰的にソートし結合

def marge(a):  #マージソート
    if len(a) <= 1:
        return a
    
    dev = int(len(a)/2)
    F = marge(a[:dev])
    B = marge(a[dev:])  #配列を前後でに分割し、再帰的にそれぞれをソートする
    Ans = []

    while True:
        if len(F) == 0:
            if len(B) == 0:
                break
            else:
                Ans.append(B.pop(0))
        elif len(B) == 0:
            Ans.append(F.pop(0))
        elif F[0] < B[0]:
            Ans.append(F.pop(0))
        else:
            Ans.append(B.pop(0))  #先頭を比較して小さいほうを取り出す
       #ここの記述はもっと簡単になるような気がします
    return Ans
    

list = [4,39,85,-2,13,3,63,9,20]
print(list)
print (quick(list))
