import sys

"""
#Homework 4-1
def Fibon(n):
    if (n == 1) | (n == 2):
        return 1
    else:
        return Fibon(n-1) + Fibon(n-2)
"""

#Homework 4-2
def Fibon(n):
    nums = [1,1]
    if n < 3:
        return nums[n-1]
    else:
        for i in range (n-2):
            temp = nums[0] + nums[1]
            nums[0] = nums[1]
            nums[1] = temp

        return temp
   
print (Fibon(int(sys.argv[1])))
    
"""
再起関数を用いるほうを  　A,
再起関数を用いないほうを　B　 として、考察を行う

Aはコードがフィボナッチ数列の定義と照らし合わせて直感的に理解しやすい。
一方Bは一目ではフィボナッチ数列の計算をしているとはわかりにくい。

Bはどれだけnが大きくなっても3要素を使いまわすだけなので、必要なメモリの量は変わらない
一方Aはnが大きくなると上位の関数が未処理で残るためにメモリが必要となりそうである。
これくらいの作業量なら問題なさそうだが複雑な再起関数を深くまで行うとよくないかもしれない(?)

処理時間はBのほうがほんの少しだけ煩雑だが、たかだかO(n)なのでそこまで差はないと思われる。
"""
